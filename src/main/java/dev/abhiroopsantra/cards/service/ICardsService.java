package dev.abhiroopsantra.cards.service;

import dev.abhiroopsantra.cards.dto.CardsDto;

public interface ICardsService {
    /**
     * Create a new card for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     */
    void createCard(String mobileNumber);

    /**
     * Fetch the card details for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     * @return Card details for the given mobile number
     */
    CardsDto fetchCard(String mobileNumber);

    /**
     * Update the card details for the given mobile number
     *
     * @param cardsDto - the request object
     * @return true if the card details are updated successfully
     */
    boolean updateCard(CardsDto cardsDto);

    /**
     * Delete the card details for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     * @return true if the card details are deleted successfully
     */
    boolean deleteCard(String mobileNumber);
}
