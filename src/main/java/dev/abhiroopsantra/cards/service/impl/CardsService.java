package dev.abhiroopsantra.cards.service.impl;

import dev.abhiroopsantra.cards.constants.CardsConstants;
import dev.abhiroopsantra.cards.dto.CardsDto;
import dev.abhiroopsantra.cards.entity.Cards;
import dev.abhiroopsantra.cards.exception.CardAlreadyExistsException;
import dev.abhiroopsantra.cards.exception.ResourceNotFoundException;
import dev.abhiroopsantra.cards.mapper.CardsMapper;
import dev.abhiroopsantra.cards.repository.CardsRepository;
import dev.abhiroopsantra.cards.service.ICardsService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service @AllArgsConstructor public class CardsService implements ICardsService {

    private CardsRepository cardsRepository;

    /**
     * Create a new card for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     */
    @Override public void createCard(String mobileNumber) {

        Optional<Cards> existingCard = cardsRepository.findByMobileNumber(mobileNumber);
        if (existingCard.isPresent()) {
            throw new CardAlreadyExistsException("Card already exists for the given mobile number: " + mobileNumber);
        }

        Cards newCard = createNewCard(mobileNumber);
        cardsRepository.save(newCard);
    }

    private Cards createNewCard(String mobileNumber) {
        Cards newCard          = new Cards();
        long  randomCardNumber = 100000000000L + new Random().nextInt(900000000);
        newCard.setCardId(randomCardNumber);
        newCard.setCardNumber(String.valueOf(randomCardNumber));
        newCard.setMobileNumber(mobileNumber);
        newCard.setCardType(CardsConstants.CREDIT_CARD);
        newCard.setTotalLimit(CardsConstants.NEW_CARD_LIMIT);
        newCard.setAmountUsed(0f);
        newCard.setAvailableAmount(CardsConstants.NEW_CARD_LIMIT);

        return newCard;
    }

    /**
     * Fetch the card details for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     * @return Card details for the given mobile number
     */
    @Override public CardsDto fetchCard(String mobileNumber) {
        Cards card = cardsRepository.findByMobileNumber(mobileNumber).orElseThrow(
                () -> new ResourceNotFoundException("Card", "MobileNumber", mobileNumber));

        return CardsMapper.mapToCardsDto(card, new CardsDto());
    }

    /**
     * Update the card details for the given mobile number
     *
     * @param cardsDto - the request object
     * @return true if the card details are updated successfully
     */
    @Override public boolean updateCard(CardsDto cardsDto) {
        Cards card = cardsRepository.findByCardNumber(cardsDto.getCardNumber()).orElseThrow(
                () -> new ResourceNotFoundException("Card", "CardNumber", cardsDto.getCardNumber()));

        CardsMapper.mapToCards(cardsDto, card);
        cardsRepository.save(card);

        return true;
    }

    /**
     * Delete the card details for the given mobile number
     *
     * @param mobileNumber - the mobile number of the customer
     * @return true if the card details are deleted successfully
     */
    @Override public boolean deleteCard(String mobileNumber) {
        Cards card = cardsRepository.findByMobileNumber(mobileNumber).orElseThrow(
                () -> new ResourceNotFoundException("Card", "MobileNumber", mobileNumber));

        cardsRepository.deleteById(card.getCardId());

        return true;
    }
}
