package dev.abhiroopsantra.cards.controller;

import dev.abhiroopsantra.cards.constants.CardsConstants;
import dev.abhiroopsantra.cards.dto.CardsContactInfoDto;
import dev.abhiroopsantra.cards.dto.CardsDto;
import dev.abhiroopsantra.cards.dto.ResponseDto;
import dev.abhiroopsantra.cards.service.ICardsService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController @Validated @RequestMapping(path = "/api", produces = {MediaType.APPLICATION_JSON_VALUE})
public class CardsController {
    private static final Logger              logger = LoggerFactory.getLogger(CardsController.class);
    private final        ICardsService       cardsService;
    private final        Environment         environment;
    private final        CardsContactInfoDto cardsContactInfoDto;
    @Value("${build.version}")
    private              String              buildVersion;

    public CardsController(
            ICardsService cardsService, Environment environment, CardsContactInfoDto cardsContactInfoDto
                          ) {
        this.cardsService        = cardsService;
        this.environment         = environment;
        this.cardsContactInfoDto = cardsContactInfoDto;
    }

    @PostMapping("/create") public ResponseEntity<ResponseDto> createCard(
            @Valid @RequestParam @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile Number must be 10 digits")
            String mobileNumber
                                                                         ) {

        cardsService.createCard(mobileNumber);

        return ResponseEntity.status(HttpStatus.CREATED)
                             .body(new ResponseDto(CardsConstants.STATUS_201, CardsConstants.MESSAGE_201));

    }

    @GetMapping("/fetch") public ResponseEntity<CardsDto> fetchCard(
            @RequestHeader("ezbank-correlation-id") String correlationId,
            @RequestParam @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile Number must be 10 digits")
            String mobileNumber
                                                                   ) {
        logger.debug("ezbank-correlation-id: {}", correlationId);
        CardsDto cardsDto = cardsService.fetchCard(mobileNumber);

        return ResponseEntity.status(HttpStatus.OK).body(cardsDto);

    }

    @PutMapping("/update") public ResponseEntity<ResponseDto> updateCardDetails(@Valid @RequestBody CardsDto cardsDto) {
        boolean isUpdated = cardsService.updateCard(cardsDto);

        if (isUpdated) {
            return ResponseEntity.status(HttpStatus.OK)
                                 .body(new ResponseDto(CardsConstants.STATUS_200, CardsConstants.MESSAGE_200));
        } else {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                                 .body(new ResponseDto(CardsConstants.STATUS_417, CardsConstants.MESSAGE_417_UPDATE));
        }
    }

    @DeleteMapping("/delete") public ResponseEntity<ResponseDto> deleteCardDetails(
            @RequestParam @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile number must be 10 digits")
            String mobileNumber
                                                                                  ) {
        boolean isDeleted = cardsService.deleteCard(mobileNumber);
        if (isDeleted) {
            return ResponseEntity.status(HttpStatus.OK)
                                 .body(new ResponseDto(CardsConstants.STATUS_200, CardsConstants.MESSAGE_200));
        } else {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                                 .body(new ResponseDto(CardsConstants.STATUS_417, CardsConstants.MESSAGE_417_DELETE));
        }
    }

    @GetMapping("/build-info") public ResponseEntity<String> getBuildInfo() {
        return ResponseEntity.status(HttpStatus.OK).body(buildVersion);
    }

    @GetMapping("/java-version") public ResponseEntity<String> getJavaVersion() {
        return ResponseEntity.status(HttpStatus.OK).body(environment.getProperty("JAVA_HOME"));
    }

    @GetMapping("/contact-info") public ResponseEntity<CardsContactInfoDto> getContactInfo() {
        return ResponseEntity.status(HttpStatus.OK).body(cardsContactInfoDto);
    }
}
