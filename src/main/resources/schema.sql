-- SELECT 'CREATE DATABASE ezbank_cards'
-- WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'ezbank_cards')
-- \gexec;

create table if not exists cards
(
    card_id          bigserial
        primary key,
    created_at       timestamp(6),
    created_by       varchar(255),
    updated_at       timestamp(6),
    updated_by       varchar(255),
    amount_used      real not null,
    available_amount real not null,
    card_number      varchar(255),
    card_type        varchar(255),
    mobile_number    varchar(255),
    total_limit      real not null
);

